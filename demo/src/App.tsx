// Copyright (c) Meta Platforms, Inc. and affiliates.
// All rights reserved.

// This source code is licensed under the license found in the
// LICENSE file in the root directory of this source tree.

import { InferenceSession, Tensor } from "onnxruntime-web";
import React, { useContext, useEffect, useState } from "react";
import "./assets/scss/App.scss";
import { handleImageScale } from "./components/helpers/scaleHelper";
import { modelScaleProps } from "./components/helpers/Interfaces";
import { onnxMaskToImage } from "./components/helpers/maskUtils";
import { modelData } from "./components/helpers/onnxModelAPI";
import Stage from "./components/Stage";
import AppContext from "./components/hooks/createContext";
const ort = require("onnxruntime-web");
/* @ts-ignore */
import npyjs from "npyjs";

// Define image, embedding and model paths
const ROOT_BATCH_PATH = "assets/data/chessdataset_temp_2/";
let CURRENT_IMAGE_ID = 1;
let IMAGE_PATH = ROOT_BATCH_PATH + String(CURRENT_IMAGE_ID) + "/" + "image.jpg";
let IMAGE_EMBEDDING = ROOT_BATCH_PATH + String(CURRENT_IMAGE_ID) + "/" + "embedding.npy";
// let IMAGE_PATH = "assets/data/STEM_JEOLHAADF_04-27-1713nmSTOp-Ge 033117LO110042617HAADF_0007.tiff";
// let IMAGE_EMBEDDING = "assets/data/tiff_image_embedding.npy";
const MIN_IMAGE_ID = 1;
const MAX_IMAGE_ID = 10;
const MODEL_DIR = "/model/sam_onnx_quantized_example.onnx";

const fs = require('fs');

// Recursive function to get files
function getFiles(dir: string, files = []) {
  const fileList = fs.readdirSync(dir);
  for (const file of fileList) {
    const name = `${dir}/${file}`;
    if (fs.statSync(name).isDirectory()) {
      getFiles(name, files);
    } else {
      // @ts-ignore
      files.push(name);
    }
  }
  return files;
}

function getFolders(dir: string, folders = []) {
  const folderList = fs.readdirSync(dir);
  for (const folder of folderList) {
    const name = `${dir}/${folder}`;
    if (fs.statSync(name).isDirectory()) {
      // @ts-ignore
      files.push(name);
    }
  }
  return folders;
}


const App = () => {
  const {
    clicks: [clicks],
    image: [, setImage],
    maskImg: [, setMaskImg],
  } = useContext(AppContext)!;
  const [model, setModel] = useState<InferenceSession | null>(null); // ONNX model
  const [tensor, setTensor] = useState<Tensor | null>(null); // Image embedding tensor
  const [maskImage, setMaskImageLocal] = useState<HTMLImageElement | null>(null);

  // The ONNX model expects the input to be rescaled to 1024. 
  // The modelScale state variable keeps track of the scale values.
  const [modelScale, setModelScale] = useState<modelScaleProps | null>(null);

  // Initialize the ONNX model. load the image, and load the SAM
  // pre-computed image embedding
  useEffect(() => {
    // Initialize the ONNX model
    const initModel = async () => {
      try {
        if (MODEL_DIR === undefined) return;
        const URL: string = MODEL_DIR;
        const model = await InferenceSession.create(URL);
        setModel(model);
      } catch (e) {
        console.log(e);
      }
    };
    initModel();

    //Initialize Folders

    const initFolders = async () => {
      //const folders = getFolders(ROOT_BATCH_PATCH);
      //console.log('Files: ', folders);
      //test_function('');
      //const folders = getFolders(ROOT_BATCH_PATH);
      //console.log(folders);
    };
    initFolders();

    // Load the image
    const url = new URL(IMAGE_PATH, location.origin);
    console.log(url.href);
    loadImage(url);

    // Load the Segment Anything pre-computed embedding
    Promise.resolve(loadNpyTensor(IMAGE_EMBEDDING, "float32")).then(
      (embedding) => setTensor(embedding)
    );
  }, []);

  const loadImage = async (url: URL) => {
    try {
      const img = new Image();
      img.src = url.href;
      img.onload = () => {
        const { height, width, samScale } = handleImageScale(img);
        setModelScale({
          height: height,  // original image height
          width: width,  // original image width
          samScale: samScale, // scaling factor for image which has been resized to longest side 1024
        });
        img.width = width; 
        img.height = height; 
        setImage(img);
      };
    } catch (error) {
      console.log('Could not load image.');
      console.log(error);
    }
  };

  // Decode a Numpy file into a tensor. 
  const loadNpyTensor = async (tensorFile: string, dType: string) => {
    let npLoader = new npyjs();
    const npArray = await npLoader.load(tensorFile);
    const tensor = new ort.Tensor(dType, npArray.data, npArray.shape);
    return tensor;
  };

  let handleSaveButtonClick = async (e: any) => {
    if (maskImage == null) {
      console.log('No mask image found.')
      return;
    }
    console.log(maskImage);

    console.log('Mask image logged above.');
    let promptString = prompt('Type in label for this mask.');
    if (promptString === null || promptString.match(/^ *$/) !== null) {
      console.log('Mask not saved: empty label string.');
      return;
    }
    let imageSrc = maskImage.currentSrc;
    const anchor = document.createElement('a');
    const savePath = ROOT_BATCH_PATH + String(CURRENT_IMAGE_ID) + 'labels/' + promptString+'.png';
    anchor.setAttribute('download', promptString+'.png'); // Set the desired file name
    anchor.href = imageSrc;
    anchor.click();
  };

  let reloadImage = async () => {
    IMAGE_EMBEDDING = ROOT_BATCH_PATH + String(CURRENT_IMAGE_ID) + "/" + "embedding.npy";
    IMAGE_PATH = ROOT_BATCH_PATH + String(CURRENT_IMAGE_ID) + "/" + "image.jpg";
    const url = new URL(IMAGE_PATH, location.origin);
    loadImage(url);
    // @ts-ignore
    setMaskImg(null);
    setMaskImageLocal(null);

    // Load the Segment Anything pre-computed embedding
    Promise.resolve(loadNpyTensor(IMAGE_EMBEDDING, "float32")).then(
      (embedding) => setTensor(embedding)
    );
  };

  let handlePrevButtonClick = async (e: any) => {
    if (CURRENT_IMAGE_ID == MIN_IMAGE_ID)
      return;
    CURRENT_IMAGE_ID = CURRENT_IMAGE_ID - 1;
    reloadImage();
    console.log('Previous Button Clicked');
  };

  let handleNextButtonClick = async (e: any) => {
    if (CURRENT_IMAGE_ID == MAX_IMAGE_ID)
      return;
    CURRENT_IMAGE_ID = CURRENT_IMAGE_ID + 1;
    reloadImage();
    console.log('Next Button Clicked');
  };



  // Run the ONNX model every time clicks has changed
  useEffect(() => {
    runONNX();
  }, [clicks]);

  const runONNX = async () => {
    try {
      if (
        model === null ||
        clicks === null ||
        tensor === null ||
        modelScale === null
      )
        return;
      else {
        // Preapre the model input in the correct format for SAM. 
        // The modelData function is from onnxModelAPI.tsx.
        const feeds = modelData({
          clicks,
          tensor,
          modelScale,
        });
        if (feeds === undefined) return;
        // Run the SAM ONNX model with the feeds returned from modelData()
        const results = await model.run(feeds);
        const output = results[model.outputNames[0]];
        // The predicted mask returned from the ONNX model is an array which is 
        // rendered as an HTML image using onnxMaskToImage() from maskUtils.tsx.
        const mask_image = onnxMaskToImage(output.data, output.dims[2], output.dims[3]);
        setMaskImageLocal(mask_image);
        setMaskImg(mask_image);
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
        <div>
          <button type="button" onClick={handleSaveButtonClick}>Save Mask</button>
          <div>
            <button type="button" onClick={handlePrevButtonClick}>Previous Image</button>
          </div>
          <div>
            <button type="button" onClick={handleNextButtonClick}>Next Image</button>
          </div>
          <Stage/>
        </div>
  );
};

export default App;
