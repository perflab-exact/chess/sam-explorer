
*About*: This is an edited version of the SAM repo, particularly with edits to the demo.  Most of the code edited was done in App.tsx, where buttons were added to load in previous/next images that are set up in a particular format, determined by a script by Waqwoya.  In addition, a button to save masks was added, and those images are saved in .png format to the downloads folder.

Further instructions on how to run are in:
```bash
segment-anything/demo/README.md
```

SAM repository:
  https://github.com/facebookresearch/segment-anything  commit: 6fdee8f2


*Authors*: Edwin Retana
